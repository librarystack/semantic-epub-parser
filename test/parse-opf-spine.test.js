import path from "path";
import SemanticEpubParser from "../src/semantic-epub-parser";

const epub3DirFixturePath = path.resolve("./test/fixtures/alice/");
const epub3HtmlPath = path.resolve("./test/fixtures/short.xhtml");

test("can extract opf spine data", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const spineData = EsEpub.parseOpfSpine();
  //   console.log("spine data", spineData);
  expect(spineData.length).toBe(13);
  spineData.forEach((item) => {
    expect(typeof item.id).toEqual(expect.any(String));
    expect(typeof item.href).toEqual(expect.any(String));
    expect(typeof item.location).toEqual(expect.any(String));
    expect(typeof item.type).toEqual(expect.any(String));
    expect(typeof item.type).toEqual(expect.any(String));
  });
});
