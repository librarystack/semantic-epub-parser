import path from "path";
import SemanticEpubParser from "../src/semantic-epub-parser";
import { normalizeHtml } from "../src/chunk-html";

const epub3DirFixturePath = path.resolve("./test/fixtures/alice/");
const epub3HtmlPath = path.resolve("./test/fixtures/short.xhtml");

test("can locate cfis with spine de-reference for html p's", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const parsedShort = await EsEpub.parseHtml(
    "OPS/short.xhtml",
    "chapter_003",
    100
  );
  expect(parsedShort.length).toBe(8);
  const parsedLong = await EsEpub.parseHtml(
    "OPS/short.xhtml",
    "chapter_003",
    1000
  );
  expect(parsedLong.length).toBe(5);
});

test("can generate valid quote prefix and suffixes", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const parsed = await EsEpub.parseHtml("OPS/short.xhtml", "chapter_003", 500);

  // Note: the text needs to be re-normalized here because each chunk may have leading
  // or trailing whitespace from previous normalization of newline characters.
  const allText = normalizeHtml(
    parsed.map((chunk) => chunk.innerText).join("")
  );

  parsed.forEach((chunk) => {
    const chunkWithContext = `${chunk.textPrefix}${chunk.innerText}${chunk.textSuffix}`;
    const found = allText.includes(chunkWithContext);
    expect(found).toBe(true);
    // if (!found) {
    //   const debug = 1;
    // }
    // console.log(chunk.textPrefix, chunk.innerText, chunk.textSuffix);
    // console.log(chunkWithContext);
    // console.log(allText);
    // console.log("-----");
  });
});

test("can generate valid cfi locators", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const parsed = await EsEpub.parseHtml("OPS/short.xhtml", "chapter_003", 500);
  expect(parsed.length).toBe(5);
});

test("can parse html as json-ld", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();

  const parsedHtml = await EsEpub.parseHtmlAsJsonLd(
    "test",
    "OPS/short.xhtml",
    "chapter_003"
  );

  console.log("json-ld", JSON.stringify(parsedHtml, null, 2));
  expect(parsedHtml.length).toBe(5);
});

test("can write parsed data to json file", async () => {
  const EsEpub = new SemanticEpubParser(
    epub3DirFixturePath,
    "./test/output/jsonld-test.json"
  );
  const result = await EsEpub.parseEpubAsJsonLd();
});
// Text("can parse all xhtml named in the spine");
