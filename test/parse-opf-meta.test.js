import path from "path";
import SemanticEpubParser from "../src/semantic-epub-parser";

const epub3DirFixturePath = path.resolve("./test/fixtures/alice/");
const epub3HtmlPath = path.resolve("./test/fixtures/short.xhtml");

test("can extract opf meta data", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const metadata = EsEpub.parseOpfMeta();
  console.log("metadata", metadata);
  expect(Object.keys(metadata).length).toBe(10);
  for (let property in metadata) {
    expect(metadata[property]).toEqual(
      expect.arrayContaining([
        expect.objectContaining({ val: expect.any(String) }),
      ])
    );
  }
});

test("can get json-ld representation of opf", async () => {
  const EsEpub = new SemanticEpubParser(epub3DirFixturePath);
  await EsEpub.loadOpf();
  const jsonLd = await EsEpub.getMetaJsonLd();
  console.log("jsonld metadata: ", JSON.stringify(jsonLd, undefined, 2));
});
