import path from "path";
import { promises as fs } from "fs";
const { JSDOM } = require("jsdom");
import {
  getChunkNodes,
  getRangesWithinLength,
  splitStringIntoPhrases,
  normalizeHtml,
} from "../src/chunk-html";

test("normalize html can clean html text", () => {
  let htmlString = "this is a string <a>with inline html</a>";
  let normalized = normalizeHtml(htmlString);
  expect(normalized).toBe("this is a string with inline html");

  htmlString = "this is a string <a>with inline html but no closing tag";
  normalized = normalizeHtml(htmlString);
  expect(normalized).toBe(
    "this is a string with inline html but no closing tag"
  );

  htmlString = "  this is a string <a>with inline html </a>  with extra spaces";
  normalized = normalizeHtml(htmlString);
  expect(normalized).toBe(
    " this is a string with inline html with extra spaces"
  );

  htmlString = `  this is a string with a new 
  line`;
  normalized = normalizeHtml(htmlString);
  expect(normalized).toBe(" this is a string with a new line");

  htmlString = "this is a string with &amp; html entities";
  normalized = normalizeHtml(htmlString);
  expect(normalized).toBe("this is a string with & html entities");
});

test("can get all chunk nodes", async () => {
  const filePath = path.resolve("./test/fixtures/short.xhtml");
  const fileData = await fs.readFile(filePath);
  const dom = await new JSDOM(fileData, {
    contentType: "application/xhtml+xml",
  });

  const chunkNodes = getChunkNodes(dom.window.document.body);
  console.log(
    "nodes",
    chunkNodes.map((node) => node.textContent)
  );
  expect(chunkNodes.length).toBe(8);
});

test("can split string without loss", () => {
  const test = "\n1\n";
  console.log("test", test.length);
  const simpleString =
    "Jived fox nymph grabs quick waltz. Glib jocks quiz nymph to vex dwarf.";
  const leadingWhiteSpaceString = "     Jived fox nymph grabs quick waltz.";
  const leadingAndTrailingWhiteSpaceString =
    "     Jived fox nymph grabs quick waltz.     ";
  const newLineString =
    "\n Jived fox nymph grabs quick waltz.   \n Glib jocks quiz nymph to vex dwarf.  \n  ";

  const simpleSplit = splitStringIntoPhrases(simpleString);
  expect(simpleSplit.join("")).toBe(simpleString);
  const leadingWhiteSpaceSplit = splitStringIntoPhrases(
    leadingWhiteSpaceString
  );
  expect(leadingWhiteSpaceSplit.join("")).toBe(leadingWhiteSpaceString);

  expect(
    splitStringIntoPhrases(leadingAndTrailingWhiteSpaceString).join("")
  ).toBe(leadingAndTrailingWhiteSpaceString);

  const splitNewLine = splitStringIntoPhrases(newLineString);
  expect(splitNewLine.join("")).toBe(newLineString);
  expect(splitNewLine.length).toBe(5);
  expect(splitNewLine.join("").length).toBe(newLineString.length);
});

test("can get all ranges", async () => {
  const filePath = path.resolve("./test/fixtures/short.xhtml");
  const fileData = await fs.readFile(filePath);
  const dom = await new JSDOM(fileData, {
    contentType: "application/xhtml+xml",
  });

  const chunkNodes = getChunkNodes(dom.window.document.body);
  const ranges = [];
  chunkNodes.forEach((node) => {
    const nodeRanges = getRangesWithinLength(node, dom.window.document, 100);
    const nodeString = node.textContent;
    console.log("expected", `->${nodeString}<-`);

    const phrases = splitStringIntoPhrases(nodeString);
    // phrases.forEach((phrase, i) => {
    //   console.log(phrase, "===", nodeRanges[i] && nodeRanges[i].toString());
    // });
    const rangeString = nodeRanges.map((range) => range.toString()).join("");
    console.log("  actual", `->${rangeString}<-`);
    console.log(rangeString.length, "===", nodeString.length);

    expect(rangeString.length).toBe(nodeString.length);
    // expect(rangeString).toBe(nodeString);
  });
});
