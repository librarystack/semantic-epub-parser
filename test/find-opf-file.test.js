import path from "path";
const { TestScheduler } = require("jest");

import findOpfFile from "../src/utils/find-opf-file";

const epub3DirFixturePath = path.resolve("./test/fixtures/alice/");

test("can find opf file", async () => {
  const opfPath = await findOpfFile(epub3DirFixturePath);
  expect(opfPath).toBe("OPS/package.opf");
});
