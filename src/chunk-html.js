import sanitizeHtml from "sanitize-html";
const normalizeWhitespace = require("normalize-html-whitespace");
const Entities = require("html-entities").AllHtmlEntities;

const semanticBlockTags = [
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "p",
  "pre",
  "ol",
  "ul",
  "dl",
];

const blacklistTags = [
  "script",
  "style",
  "iframe",
  "template",
  "button",
  "menu",
  "img",
  "link",
  "meta",
];

const inlineTags = [
  "a",
  "abbr",
  "b",
  "bdi",
  "bdo",
  "em",
  "i",
  "link",
  "q",
  "span",
  "sub",
  "sup",
];

/**
 * Attempts to split a dom node into semantic chunks that can be optimally indexed.
 * It starts by descending into the dom looking for semantic block elements (semanticBlockTags).
 * If a textNode is reached without finding a semantic block ancestor, the code ascends back
 * up the dom until it finds the nearest tag that is not an inline tag (inlineTags).
 * Tags listed blacklistTags are skipped.
 * @param {Node} parent - the root dom note to parse
 * @returns {Node[]} - an array of DOM Nodes
 */
export function getChunkNodes(parent, includeOrphanedText = true) {
  let chunks = [];

  // dom branches that have already been indexed are given the property 'touched'
  if (parent.touched) {
    console.log("touched", parent);
    return [];
  }
  // else if (parent.nodeType === Node.TEXT_NODE) {
  //   if (!includeOrphanedText) {
  //     chunks.push(parent);
  //   }
  //   return chunks;
  // }

  for (parent = parent.firstChild; parent; parent = parent.nextSibling) {
    if (parent?.touched) {
      continue;
    }

    if (semanticBlockTags.includes(parent.tagName)) {
      // if node is white-listed, take that chunk
      parent.touched = true;

      chunks.push(parent);
    } else if (blacklistTags.includes(parent.tagName)) {
      // skip
      continue;
    } else if (parent.nodeType === 3 /*Node.TEXT_NODE*/) {
      // found a text node that is not within one of the semanticBlockTags (orphaned)
      // find nearest parent that is not a inline tag
      // ignore white space
      parent.touched = true;
      if (parent.textContent.trim().length === 0) {
        continue;
      }

      // find the nearest ancestor that is not a phrasing tag and also does not contain sibling semanticBlockTags
      let textParent = parent.parentNode;
      while (textParent && inlineTags.includes(textParent.tagName)) {
        textParent = textParent.parentNode;
      }

      if (!textParent.touched) {
        // no siblings that would cause conflict - save the parent

        const hasSiblings = nodeContainsSemanticBlocks(textParent);
        if (hasSiblings === false) {
          textParent.touched = true;
          chunks.push(textParent);
        } else if (includeOrphanedText) {
          // save the orphaned text as is
          chunks.push(parent);
        }
      }
    } else {
      // traverse children
      chunks = chunks.concat(getChunkNodes(parent, includeOrphanedText));
    }
  }
  return chunks;
}

/**
 * Detect if a given Node contains any semantic block tags
 * @param {Node} parent
 * @returns {boolean} true of the parent node contains any semantic block tags
 */
function nodeContainsSemanticBlocks(parent) {
  let result = false;
  for (parent = parent.firstChild; parent; parent = parent.nextSibling) {
    if (semanticBlockTags.includes(parent.tagName)) {
      result = true;
      break;
    } else if (parent.nodeType !== 3 /*Node.TEXT_NODE*/) {
      result = nodeContainsSemanticBlocks(parent);
    }
  }
  return result;
}

function getTextNodes(parent) {
  var all = [];
  if (parent.nodeType === 3 /*Node.TEXT_NODE*/) {
    all.push(parent);
  } else {
    for (parent = parent.firstChild; parent; parent = parent.nextSibling) {
      if (["SCRIPT", "STYLE"].indexOf(parent.tagName) >= 0) {
        continue;
      }
      if (parent.nodeType === 3 /*Node.TEXT_NODE*/) {
        all.push(parent);
      } else {
        all = all.concat(getTextNodes(parent));
      }
    }
  }
  return all;
}

// https://developer.mozilla.org/en-US/docs/Web/API/Document_Object_Model/Whitespace
// https://github.com/jsdom/jsdom/issues/1245#issuecomment-470192636
// https://github.com/timdown/rangy/wiki/Text-Range-Module

/**
 * Break text up into instance of Range objects
 * https://developer.mozilla.org/en-US/docs/Web/API/Range
 * @param {Node} parent node
 * @param {document} document instance to get createRange from
 * @param {number} maxLength
 * @returns {Range[]} array of Range objects
 */
export function getRangesWithinLength(parent, document, maxLength) {
  const textNodes = getTextNodes(parent);

  const ranges = [];

  // console.log(
  //   "textNodes",
  //   textNodes.map((node) => node.textContent)
  // );
  /**
   * Split textNode textContent into phrases and build array of sub-node Range objects
   */
  if (textNodes.length > 0) {
    const subRanges = [];
    textNodes.forEach((textNode, textNodeIndex) => {
      const text = textNode.textContent;
      if (!text || text.trim().length === 0) {
        // console.log("empty node found");
        return;
      }

      // regex from: https://stackoverflow.com/a/62520865/7943589
      const phrases = splitStringIntoPhrases(text);
      // console.log(
      //   "textNode",
      //   textNode.textContent,
      //   textNode.textContent.length
      // );

      let nodeTextOffset = 0;
      phrases.forEach((phrase, phraseIndex) => {
        // console.log("phrase", phrase, phrase.length, nodeTextOffset);
        const range = document.createRange();
        range.setStart(textNode, nodeTextOffset);
        range.setEnd(textNode, nodeTextOffset + phrase.length);
        subRanges.push(range);
        nodeTextOffset += phrase.length;
      });
    });
    // console.log(
    //   "-ranges",
    //   subRanges.map((subRange) => subRange.toString())
    // );

    /**
     * Aggregate the ranges to fit within the length of maxLength
     * Texts that could not be further split are kept intact
     */
    if (subRanges.length > 0) {
      let subRangeAcc = document.createRange();
      subRangeAcc.setStart(
        subRanges[0].startContainer,
        subRanges[0].startOffset
      );
      subRanges.forEach((subRange, i) => {
        // console.log(
        //   "checking subRange",
        //   subRange.toString(),
        //   "acc:",
        //   subRangeAcc.toString()
        // );
        if (subRange.toString().length > maxLength) {
          // The string could not be split... leave intact.

          // End previous range.
          if (subRangeAcc.toString().length > 0) {
            const lastRange = getLastElement(subRanges);
            //ubRangeAcc.setEnd(lastRange.endContainer, lastRange.endOffset);
            // console.log(
            //   "ending last range before adding long",
            //   subRangeAcc.toString()
            // );
            ranges.push(subRangeAcc.cloneRange());
            subRangeAcc = document.createRange();
          }

          // collect the long range
          ranges.push(subRange.cloneRange());
          // console.log(
          //   "--- pushing long",
          //   subRange.toString(),
          //   "=>",
          //   subRangeAcc.toString()
          // );
        } else if (
          subRangeAcc.toString().length + subRange.toString().length <=
          maxLength
        ) {
          // the accumulator length will still be shorter than the maxLength.
          // Collect new phrase
          if (subRangeAcc.startContainer.isSameNode(document)) {
            // console.log("--- starting group with:", subRange.toString());
            subRangeAcc.setStart(subRange.startContainer, subRange.startOffset);
          }
          subRangeAcc.setEnd(subRange.endContainer, subRange.endOffset);
          // console.log(
          //   "--- addind to acc:",
          //   subRange.toString(),
          //   "->",
          //   subRangeAcc.toString()
          // );
        } else {
          // The accumulator length has reached maxLength. Start a new one.
          ranges.push(subRangeAcc.cloneRange());
          // console.log("--- pushing group", subRangeAcc.toString());
          subRangeAcc = document.createRange();
          subRangeAcc.setStart(subRange.startContainer, subRange.startOffset);
          subRangeAcc.setEnd(subRange.endContainer, subRange.endOffset);
        }
      });
      // add anything that is left in the accumulator
      if (subRangeAcc.toString().length > 0) {
        const lastRange = getLastElement(subRanges);
        //subRangeAcc.setEnd(lastRange.endContainer, lastRange.endOffset);
        ranges.push(subRangeAcc.cloneRange());
        // console.log("--- pushing remainder:", subRangeAcc.toString());
      }
    }
  }

  // ranges.forEach((range) =>
  //   console.log("--- aggregated subrange:", range.toString())
  // );

  return ranges;
}

/**
 * Attempts to break a multi-sentence text block in smaller semantic pieces.
 * @param {string} text - the input text string to break into pieces
 */
export function splitStringIntoPhrases(text) {
  const phrases = text.match(/[^.?!]+[.!?]+[\])'"`’”\n]*|.+|\n/gm) || [];
  return phrases;
}

/**
 * JSDOM doesn't support innerText and the W3C spec for web annotation requires
 * that quoted text be normalized. This method removes any tags, and collapses any
 * html entities back to their unicode representation.
 * @param {string} htmlString - an html string to normalize
 * @returns {string} - normalized html text contents
 */
export function normalizeHtml(htmlString) {
  try {
    const sanitizedHtml = sanitizeHtml(htmlString, {
      allowedTags: [],
      allowedAttributes: {},
      // parser: {
      //   decodeEntities: false,
      // },
    });
    const cleanedText = normalizeWhitespace(sanitizedHtml);
    const entities = new Entities();
    const decoded = entities.decode(cleanedText);
    return decoded;
  } catch (error) {
    console.error("Error normalizing html:", error);
  }
  return htmlString;
}

function getLastElement(array) {
  if (array.length === 0) {
    return;
  } else if (array.length === 1) {
    return array[0];
  } else if (array.length > 1) {
    return array[array.length - 1];
  }
}

function normalizeText(text) {
  let cleanedText = text.replace(/([\t\n\r])+/g, " ").trim();
  return cleanedText;
}
