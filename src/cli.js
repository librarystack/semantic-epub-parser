#!/usr/bin/env node
//const inquirer = require('inquirer');
//const SemanticEpubParser = require("./src/semantic-epub-parser");
import path from "path";
import inquirer from "inquirer";
import SemanticEpubParser from "./semantic-epub-parser.js";

async function run() {
  const { sourceEpubDir } = await inquirer.prompt([
    {
      name: "sourceEpubDir",
      type: "input",
      message: "What is the source epub directory?",
      filter: (answer) => {
        //console.log("answer:", answer);
        if (answer) {
          let cleanAnswer = answer.trim().replace(/\\ /g, " ");
          cleanAnswer = cleanAnswer.replace(/'/g, "");
          return cleanAnswer;
        }
        return answer;
      },
    },
  ]);

  const fileName = `${path.basename(sourceEpubDir)}.epub.json`;
  const defaultPath = path.resolve(sourceEpubDir, "..", fileName);

  const { destPath } = await inquirer.prompt([
    {
      name: "destPath",
      type: "input",
      message: "Where do you want to save the parsed epub data?",
      default: defaultPath,
      filter: (answer) => {
        if (answer) {
          let cleanAnswer = answer.trim().replace(/\\ /g, " ");
          cleanAnswer = cleanAnswer.replace(/'/g, "");
          //console.log("answer:", cleanAnswer);
          return cleanAnswer;
        }
        return answer;
      },
    },
  ]);

  try {
    const parser = new SemanticEpubParser(sourceEpubDir, destPath);
    parser.loadOpf();
    await parser.parseEpubAsJsonLd();
    console.log("Epub parsing completed.");
    console.log("Epub data saved to ", destPath);
  } catch (error) {
    console.error("Error parsing epub:", error);
  }
}
run();
