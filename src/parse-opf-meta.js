// https://github.com/readium/webpub-manifest
// https://json-ld.org/spec/ED/json-ld-syntax/20111016/
// https://www.npmjs.com/package/jsonld

export default function parseOpfMeta(opfData) {
  const metadata = Object.assign({}, opfData.package.metadata[0]);

  if (metadata.meta) {
    metadata.meta.forEach((meta) => {
      if (meta.attr?.property) {
        const name = meta.attr.property;
        const value = meta.val;
        const refines = meta.attr?.refines;

        if (refines) {
          refine(metadata, meta);
        } else {
          if (metadata[name]) {
            metadata[name].push({ val: value });
          } else {
            metadata[name] = [{ val: value }];
          }
        }
      }
    });
    delete metadata.meta;
  }

  // hoist attributes to be properties of the object
  for (let key in metadata) {
    if (Array.isArray(metadata[key])) {
      metadata[key].forEach((element) => {
        if (element.attr) {
          for (let attrKey in element.attr) {
            element[attrKey] = element.attr[attrKey];
          }
          delete element.attr;
        }
      });
    }
  }
  metadata["xmlns:dc"] = [{ val: metadata.attr["xmlns:dc"] }];
  delete metadata.attr;
  return metadata;
}

function refine(metadata, meta) {
  //metadata.findIndex(datum => id === datum.attr.id );
  const id = meta.attr.refines.replace("#", "");
  const prop = meta.attr.property;
  const value = meta.val;

  for (const property in metadata) {
    if (property.attr?.id === id) {
      if (property[prop]) {
        property[prop].push({ val: value });
      } else {
        property[prop] = value;
      }
    }
  }
}

/**
 * Create a compacted json-ld representation of the opf metadata.
 * see:
 * https://json-ld.org/playground/?startTab=tab-compacted&json-ld=%7B%22%40context%22%3A%7B%22nc%22%3A%22http%3A%2F%2Frelease.niem.gov%2Fniem%2Fniem-core%2F4.0%2F%23%22%2C%22j%22%3A%22http%3A%2F%2Frelease.niem.gov%2Fniem%2Fdomains%2Fjxdm%2F6.0%2F%23%22%7D%2C%22nc%3APersonAgeMeasure%22%3A%7B%22nc%3AMeasureIntegerValue%22%3A14%2C%22nc%3ATimeUnitCode%22%3A%22ANN%22%7D%2C%22j%3APersonHairColorCode%22%3A%22BRO%22%2C%22nc%3APersonName%22%3A%7B%22nc%3APersonGivenName%22%3A%22Mortimer%22%2C%22nc%3APersonSurName%22%3A%22Smith%22%2C%22nc%3APersonNameSuffixText%22%3A%22Sr%22%2C%22nc%3APersonPreferredName%22%3A%22Morty%22%7D%7D&frame=%7B%7D&context=%7B%7D
 * @param {object} opfData xml2Js representation of the opf xml
 * @returns {object} an object that can be serialized into json
 */
export async function getJsonLd(opfData) {
  const data = parseOpfMeta(opfData);

  const graph = {};
  for (let prop in data) {
    if (Array.isArray(data[prop])) {
      data[prop].forEach((datum) => {
        if (graph[prop]) {
          graph[prop].push(datum.val);
        } else {
          graph[prop] = [datum.val];
        }
      });
    }
  }

  // the web annotation context already has definitions for dc:
  // graph["@context"] = {
  //   dc: data["xmlns:dc"][0].val,
  // };
  delete graph["xmlns:dc"];
  graph["id"] = data["dc:identifier"][0].val;
  const jsonldData = graph;
  //const expadnedJsonldData = await jsonld.expand(graph);

  return jsonldData;
}
