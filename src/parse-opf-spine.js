import path from "path";

export default function parseOpfSpine(opfLocation, opfData) {
  const spine = opfData.package.spine[0];
  const manifestItems = opfData.package.manifest[0].item;
  const itemrefs = spine.itemref;

  const sections = itemrefs.map((itemref) => {
    const idref = itemref.attr.idref;
    const manifestItem = manifestItems.find((item) => item.attr.id === idref);
    const href = manifestItem.attr.href;
    // resolve the file location relative to the epub root
    const location = path.join(path.dirname(opfLocation), href);
    return {
      id: idref,
      href: manifestItem.attr.href,
      location: location,
      type: manifestItem.attr["media-type"],
      properties: manifestItem.attr?.properties
        ? manifestItem.attr.properties
        : "",
    };
  });

  return sections;
}
