export function findAllIndexes(needle, haystack) {
  let indexes = [];
  let index = 0;
  console.log('findAllIndexes', needle, haystack);
  while (index !== -1) {
    index = haystack.indexOf(needle, index);
    if (index !== -1) {
      indexes.push(index);

      // avoid infinite loop if needle is only one char
      if(needle.length > 1) {
        index += needle.length - 1;
      } else {
        index++;
      }
      
    }
  }

  return indexes;
}
