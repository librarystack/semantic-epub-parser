import path from "path";
import fs from "fs-extra";

// TODO: Test https://www.npmjs.com/package/detect-is-it-html-or-xhtml

/**
 * Detects if a text file is xhtml
 * @param {string} inputPath - path to html/xhtml file
 * @returns {boolean} - true if file is xhtml
 */
export default function isXhtml(inputPath) {
  // check file extension
  if (path.extname(inputPath) == ".xhtml") {
    return true;
  }
  try {
    const inputFile = fs.readFileSync(inputPath, "utf8");
    const docTypeRegex = /<\s*!\s*doctype[^>]*>/im;
    const xmlPrologRegex = /<\?xml [^>]*\?>/im;
    const xhtmlRegex = /xhtml/gi;

    // check for xhtml in doctype tag
    const docTypeMatch = inputFile.match(docTypeRegex);
    if (docTypeMatch && docTypeMatch[0].match(xhtmlRegex)) {
      return true;
    }

    // check for XML prolog
    const xmlPrologMatch = inputFile.match(xmlPrologRegex);
    if (xmlPrologMatch) {
      return true;
    }
  } catch (e) {
    console.log("error reading file: ", e);
    return;
  }

  return false;
}
