import { promises as fs } from 'fs';
import path from "path";

/**
 * Recurse through directory and create a flat index of all the file paths found
 * @param {string} directoryName 
 * @param {array} _results 
 */
export default async function findAllFiles(directoryName, _results = []) {
  let files;
  if (directoryName === "..") {
    return;
  }

  try {
    files = await readDir(directoryName);
  } catch (err) {
    console.error("Error reading directory", directoryName, err);
    return _results;
  }

  for (let file of files) {
    const fullPath = path.join(directoryName, file);
    const stats = await fs.stat(fullPath);

    if (file !== "." && stats.isDirectory() ) {
      findAllFiles(fullPath, _results);
    } else {
      _results.push(fullPath);
    }
  }
  return _results;
}

/**
 * Get the contents of a directory
 * @param {string} directory
 * @returns {array}
 */
export async function readDir(directory) {
  return new Promise((resolve, reject) => {
    fs.readdir(directory, (err, content) => {
      if (err) {
        reject(err);
      } else {
        resolve(content);
      }
    });
  });
}
