import { promises as fs } from "fs";
import xml2js from "xml2js";
import { parseOptions } from "./xml2js-options.js";

/**
 * Read a XML file and parse it into a json object using xml2js
 * @param {string} - location
 * @returns {object} - a json object
 */
export default async function parseXmlFile(location) {
  let data;

  try {
    data = await fs.readFile(location);
  } catch (err) {
    console.warn("Could not readFile", location, err);
    return;
  }

  if (data) {
    try {
      const xml2JsParser = new xml2js.Parser(parseOptions);
      const result = await xml2JsParser.parseStringPromise(data);
      return result;
    } catch (err) {
      console.warn("Error parsing xml file:", location, err);
      return;
    }
  }
}
