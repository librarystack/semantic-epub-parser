import path from "path";
import { promises as fs } from "fs";

import parseXmlFile from "./parse-xml-file.js";

/**
 * The container.xml typically contains a single rootfile element pointing to the opf
 */
export default async function findOpfFile(epubDirPath) {
  const containerFilePath = path.resolve(
    epubDirPath,
    "./META-INF/container.xml"
  );

  const stats = await fs.stat(containerFilePath);

  if (stats.isFile()) {
    const result = await parseXmlFile(containerFilePath);

    if (result) {
      if (!result?.container) {
        console.warn("container not found in container.xml", result);
        return;
      }
      const rawRootfiles = result.container.rootfiles[0].rootfile;
      const rootfileDataList = rawRootfiles.map((rootfile) => {
        return rootfile.attr;
      });
      const rootFilePaths = rootfileDataList.map((itemData) => {
        const { "full-path": fullPath, "media-type": mediaType } = itemData;
        return fullPath;
      });

      if (rootFilePaths.length < 1) {
        console.warn(`Rootfile not listed in container.xml.`);
        return;
      } else if (rootFilePaths.length > 1) {
        console.warn(
          `More than on rootfile listed in container.xml. Using ${rootFilePaths[0]}.`
        );
      }

      const rootPath = rootFilePaths[0];
      return rootPath;
    }
  } else {
    console.warn(`container.xml not found in ${epubDirPath}`);
    return;
  }
}
