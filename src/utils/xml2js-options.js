export const parseOptions = {
  attrkey: "attr",
  charkey: "val",
  explicitCharkey: true,
  normalizeTags: true,
  trim: true,
  async: true,
  // TODO: test async: true
};

export const buildOptions = {
  attrkey: "attr",
  charkey: "val",
  xmldec: { version: "1.0", encoding: "UTF-8" },
};
