import puppeteer from "puppeteer";

const getDom = async (htmlString) => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.setContent(htmlString);
  const dom = await page.content();
};

export default getDom;
