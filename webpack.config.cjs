const path = require("path");
const ShebangPlugin = require("webpack-shebang-plugin");

const config = {
  entry: path.resolve(__dirname, "/src/cli.js"),
  target: "node",
  node: {
    __dirname: false,
    __filename: false,
  },
  output: {
    filename: "cli.js",
    path: path.resolve(__dirname, "dist"),
    scriptType: "text/javascript",
  },
  plugins: [new ShebangPlugin()],
  externals: {
    canvas: "{}",
  },
  module: {
    rules: [
      {
        test: /\.m?js$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [["@babel/preset-env", { targets: { node: true } }]],
          },
        },
      },
    ],
  },
};

module.exports = (env, argv) => {
  if (argv.mode === "development") {
    config.devtool = "inline-cheap-source-map";
  }
  if (argv.mode === "production") {
    config.devtool = "source-map";
  }
  return config;
};
