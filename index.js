// import SemanticEpubParser from "./src/semantic-epub-parser";

export {
  normalizeHtml,
  splitStringIntoPhrases,
  getRangesWithinLength,
  getChunkNodes,
} from "./src/chunk-html.js";
export { default as SemanticEpubParser } from "./src/semantic-epub-parser";
